//POS tests for the checkout object

var pos = require('./pos');

var prices = {A: 25, B: 40, P: 30};

var tests = [
    {
        testName: "shouldCalculateCorrectly_AB",
        testData: ['A', 'B'],
        expectedValue: 65
    },
    {
        testName: "shouldCalculateCorrectly_AA",
        testData: ['A', 'A'],
        expectedValue: 50
    },
    {
        testName: "shouldCalculateCorrectly_BB",
        testData: ['B', 'B'],
        expectedValue: 80
    },
    {
        testName: "shouldCalculateCorrectly_PP",
        testData: ['P', 'P'],
        expectedValue: 60
    },
    {
        testName: "shouldApplyAppleOfferCorrectly_AAA",
        testData: ['A', 'A', 'A'],
        expectedValue: 50
    },
    {
        testName: "shouldApplyAppleOfferCorrectly_AAA,AA",
        testData: ['A', 'A', 'A', 'A', 'A'],
        expectedValue: 100
    },
    {
        testName: "shouldApplyAppleOfferCorrectly_AAA,AAA",
        testData: ['A', 'A', 'A', 'A', 'A', 'A'],
        expectedValue: 100
    },
    {
        testName: "shouldApplyBananaOfferCorrectly_BBB",
        testData: ['B', 'B', 'B'],
        expectedValue: 100
    },
    {
        testName: "shouldApplyBananaOfferCorrectly_BBB,B",
        testData: ['B', 'B', 'B', 'B'],
        expectedValue: 140
    },
    {
        testName: "shouldCalculateCorrectly_BABPB",
        testData: ['B', 'A', 'B', 'P', 'B'],
        expectedValue: 155
    },
    {
        testName: "shouldCalculateCorrectly_BBAPPBAAP",
        testData: ['B', 'B', 'A', 'P', 'P', 'B', 'A', 'A', 'P'],
        expectedValue: 240
    },
    {
        testName: "shouldCalculateCorrectly_BBAPPBAAPA",
        testData: ['B', 'B', 'A', 'P', 'P', 'B', 'A', 'A', 'P', 'A'],
        expectedValue: 265
    },
    {
        testName: "shouldScanAndCalculate_PPAB",
        itemsToScan: ['P', 'P', 'A', 'B'],
        expectedValue: 125
    },
    {
        testName: "shouldScanAndCalculate_PPBBB",
        itemsToScan: ['P', 'P', 'B', 'B', 'B'],
        expectedValue: 160
    },
    {
        testName: "shouldScanAndCalculate_PPAAA",
        itemsToScan: ['P', 'P', 'A', 'A', 'A'],
        expectedValue: 110
    },
    {
        testName: "shouldScanAndCalculate_AAA",
        itemsToScan: ['A', 'A', 'A'],
        expectedValue: 50
    },
    {
        testName: "shouldScanAndCalculate_AAA,A",
        itemsToScan: ['A', 'A', 'A', 'A'],
        expectedValue: 75
    },
    {
        testName: "shouldScanAndCalculate_BBB",
        itemsToScan: ['B', 'B', 'B'],
        expectedValue: 100
    },
    {
        testName: "shouldScanAndCalculate_BBB,B",
        itemsToScan: ['B', 'B', 'B', 'B'],
        expectedValue: 140
    },
    {
        testName: "shouldScanAndCalculate_BABPB",
        itemsToScan: ['B', 'A', 'B', 'P', 'B'],
        expectedValue: 155
    }
];

testRunner = function() {
    console.log("\nPOS test initiating, " + tests.length + " tests to run...\n");

    var passedTests = 0;

    for (var i = 0; i < tests.length; i++) {
        var testValue = 0;
        //To test the Checkout object
        if (tests[i].itemsToScan) {
            //Create a new checkout object for every test
            var checkout = new pos.Checkout(prices);
            //If we have items to scan we go through each...
            for (var j = 0; j < tests[i].itemsToScan.length; j++) {
                //...and apply the scan method to each item to scan in
                checkout.scan(tests[i].itemsToScan[j]);
            }
            testValue = checkout.total();   //Get checkout total
        //To test the checkout function
        } else {
            testValue = pos.checkout(tests[i].testData, prices);
        }

        var testResult = testValue === tests[i].expectedValue;
        if (testResult) {   //Test passes
            passedTests ++;
            console.log("  " + tests[i].testName + " > passed");
        } else {    //Test fails
            console.error("x " + tests[i].testName +
            " >>> failed. Got: " + testValue +
            " but expected: " + tests[i].expectedValue);
        }
    }
    console.log("\n" + passedTests + " of " + tests.length + " tests have passed. \n");
};

testRunner();
