//The checkout function
var checkout = function(products, prices) {
    return calculateItemTotal(products, prices);
};

//The checkout object
var Checkout = function(prices) {

    var products = [];

    this.scan = function(itemCode){
        products.push(itemCode);
    };

    this.total = function() {
        return calculateItemTotal(products, prices);
    };
};

calculateItemTotal = function(products, prices) {
    //Go through all products
    var totalCost = 0;
    var totalApples = 0;
    var totalBananas = 0;
    var BANANA_OFFER = 100;

    for (var i = 0; i < products.length; i++) {
        //Add up all items and get their prices
        if (products[i] === 'A') {
            totalApples ++;
            //If we encounter a third apple...
            if ( (totalApples > 2) && ((totalApples % 3) === 0) ) {
                totalCost -= prices.A;  //...we remove the price of an apple from the total
            }
        } else if (products[i] === 'B') {
            totalBananas ++;
            //If we encounter a third banana...
            if ( (totalBananas >= 3) && ((totalBananas % 3) === 0) ) {
                //...we reduce the total cost of three bananas by the offer
                totalCost -= (prices.B*3 - BANANA_OFFER);
            }
        }
        totalCost += prices[products[i]];
    }
    return totalCost;
};

module.exports = {
    checkout: checkout,
    Checkout: Checkout
};
